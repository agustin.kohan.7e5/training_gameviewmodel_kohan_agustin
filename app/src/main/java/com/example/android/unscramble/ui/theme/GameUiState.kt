package com.example.android.unscramble.ui.theme

data class GameUiState(val currentScrambledWord: String = "")
